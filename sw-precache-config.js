var filesToCache = [
    'index.html',
    'images/**/**.*',
    'images/**.*',
    'scripts/**.js',
    'styles/**.css'
]

var apiUrl = /^https:\/\/api-ratp\.pierre-grimaud\.fr\/v3\/schedules/

module.exports = {
    staticFileGlobs: filesToCache,
    runtimeCaching: [{
        urlPattern: apiUrl,
        handler: 'cacheFirst',
    }]
}