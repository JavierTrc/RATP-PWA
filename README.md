# RATP-PWA

Small progressive web app based on the google code lab for PWA. It uses the API in the following repository to obtain the metro information [Horaires RATP API](https://github.com/pgrimaud/horaires-ratp-api). Used [Google Lighthouse](https://developers.google.com/web/tools/lighthouse/) to test for PWA metrics.

## Run locally
You can run the files with any web server that allows you to show files from the file directory. We used web server for chrome, but there are other pieces of software that can be used like hhtp-server for npm

## Firebase deployment
It is available in the following [URL](https://ratp-pwa-9a885.firebaseapp.com/)